
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "curtime.h"

// Just some sample data, we don't have time to implement
// something more fancy... ;)
#define ARR_LENGTH 8
float src1[ARR_LENGTH] = { 1, 4, 2.3, 4.5, 18.2, 2, 0, -1.2 };
float src2[ARR_LENGTH] = { 2, 3.4, 1, 5, 1.2, 13.2, 3.3, 12 };
float dest[ARR_LENGTH];

// Helper function to print a float array.
static void print_array(int n, float* array) {
    for (int i = 0; i < n && i < 12; i++) printf(" %.3f", array[i]);
    if (n > 12) printf(" ...");
    printf("\n");
}

void test_impl(void (*func)(int, float*, float*, float*), size_t repetitions) {
    // TODO: Measure time!

    memset(dest, 0, sizeof(dest));

    print_array(ARR_LENGTH, src1);
    print_array(ARR_LENGTH, src2);

	double before = curtime();
	
    for (size_t i = 0; i < repetitions; i++) {
        func(ARR_LENGTH, dest, src1, src2);
	}
	
	double after = curtime();
	
    print_array(ARR_LENGTH, dest);

    double elapsed_time = after - before;
    printf("Time taken: %f\n", elapsed_time);
}

// Scalar implementation for your convenience.
void scalar_add(int n, float* dest, float* src1, float* src2) {
    do { *dest++ = *src1++ + *src2++; } while (--n);
}

// Declarations
extern void simd_add(int, float*, float*, float*);
extern void simd_mean(int, float*, float*, float*);
extern void simd_min(int, float*, float*, float*);

#define test(func,...) do { puts(#func); test_impl(func, ## __VA_ARGS__); } while (0)

int main(int argc, char** argv) {
    int opt;
    size_t repetitions = 1;

    // $ man getopt.3
    while ((opt = getopt(argc, argv, "r:")) != -1) {
        switch (opt) {
            case 'r': repetitions = strtoul(optarg, NULL, 0); break;
            default:
                fprintf(stderr, "usage: %s [-r repetitions]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    test(scalar_add, repetitions);
    test(simd_add, repetitions);
    test(simd_mean, repetitions);
    test(simd_min, repetitions);

    return EXIT_SUCCESS;
}
