
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "curtime.h"

// Just some sample data, we were too lazy to implement
// something more fancy... ;)
#define ARR_LENGTH 8
float src[ARR_LENGTH] = { 1, 4, 2.3, 4.5, 18.2, 2, 0, -1.2 };

// Helper function to print a float array.
static void print_array(int n, float* array) {
    for (int i = 0; i < n && i < 12; i++) printf(" %.3f", array[i]);
    if (n > 12) printf(" ...");
    printf("\n");
}

void test_impl(float (*func)(int, float*), size_t repetitions) {
    // TODO: Measure time!

    print_array(ARR_LENGTH, src);

    float res = 0;
    for (size_t i = 0; i < repetitions; i++)
        res = func(ARR_LENGTH, src);

    printf("Result is: %f\n", res);

    double elapsed_time = 0;
    printf("Time taken: %f\n", elapsed_time);
}

// Declarations
extern float simd_hmin(int, float*);
extern float scalar_hmin(int, float*);

#define test(func,...) do { puts(#func); test_impl(func, ## __VA_ARGS__); } while (0)

int main(int argc, char** argv) {
    int opt;
    size_t repetitions = 1;

    // $ man getopt.3
    while ((opt = getopt(argc, argv, "r:")) != -1) {
        switch (opt) {
            case 'r': repetitions = strtoul(optarg, NULL, 0); break;
            default:
                fprintf(stderr, "usage: %s [-r repetitions]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    test(simd_hmin, repetitions);
    test(scalar_hmin, repetitions);

    return EXIT_SUCCESS;
}
