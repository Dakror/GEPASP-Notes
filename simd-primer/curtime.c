#include "curtime.h"
#include <time.h>

double curtime(void) {
    struct timespec tp;
	if (clock_gettime(CLOCK_MONOTONIC_RAW, &tp) == 0) {
		return tp.tv_sec + tp.tv_nsec * 1e-9;
	}
    return 0;
}
