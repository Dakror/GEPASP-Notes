.global unknown_func

unknown_func:
    // r0 counter und am ende ergebnis
	// r1 2k+1 wert
	// r2 -1
  	// s0 teilergebnis und dann endergebnis
	// s1 4.0
	// s2 float value von r1
	// s3 ergebnis von vdiv

	
	push {lr}
	mov r1, #0
	mov r2, #-1
    vmov s0, r1
	vcvt.f32.u32 s0, s0
    vmov.f32 s1, #4.0
    
	// if n is zero return here 
	cmp r0, #0
	bgt loop
	vmov s0, s1
	pop {pc}
	
	// loop
	loop:
		// mal 2
		lsl r1, r0, #1
		// +1
		add r1, r1, #1
		
		//mulgt r1, r1, r2
		
		vmov s2, r1
		vcvt.f32.s32 s2, s2
		
		vdiv.f32 s3, s1, s2
		
		tst r0, #1
			vaddeq.f32 s0, s0, s3
			vsubne.f32 s0, s0, s3
		
		subs r0, r0, #1
		bge loop
	
	pop {pc}
