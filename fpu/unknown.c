#include <stdio.h>
#include <stdlib.h>

extern float unknown_func(int x);

int main(int argc, char **argv) {
    int x = 1000000;
    if (argc >= 2)
        x = strtoul(argv[1], NULL, 0);
    float r = 0.0;

    r = unknown_func(x);
    printf("Result: %f\n", r);

    return 0;
}

