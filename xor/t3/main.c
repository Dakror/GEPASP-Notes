#include<stdio.h>
#include<stdlib.h>

extern int xor_cipher(char *str, int key);

int main(int argc, char** argv) {
	// put your code here.
	if(argc != 3) {
		return -1;
	}
	int returnValue = xor_cipher(argv[1], atoi(argv[2]));
	
	printf("%s (%d)\n", argv[1], returnValue);

	// return with success
	return EXIT_SUCCESS;
}
