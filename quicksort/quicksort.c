#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void quicksort_asm(int*, int, int);

void quicksort_c(int* array, int low, int high)
{
	if (low >= high) return;
	int tmp;
	int pivot = array[high];
	int smallerIdx = low; // index of elements smaller than pivot
	for (int j = low; j < high; j++)
	{
		if (array[j] <= pivot)
		{
			// swap array[j] and array[smallerIdx]
			tmp = array[j];
			array[j] = array[smallerIdx];
			array[smallerIdx] = tmp;
			smallerIdx++;
		}
	}
	// swap pivot and array[smallerIdx]
	array[high] = array[smallerIdx];
	array[smallerIdx] = pivot;
	quicksort_c(array, low, smallerIdx - 1);
	quicksort_c(array, smallerIdx + 1, high);
}

void test(void(* function)(int*, int, int), char* name)
{
	int test_array[10] = { 90, 1, 7, 4, 13, 42, -1, 0, 12, 15 };

	// copy the test array to the heap, so that memory errors
	// may be found with valgrind
	int* heaparray = malloc(sizeof(test_array));
	memcpy(heaparray, test_array, sizeof(test_array));

	function(heaparray, 0, (sizeof(test_array) / sizeof(int)) - 1);

	printf("%-3s :", name);
	for (int i = 0; i < sizeof(test_array) / sizeof(int); i++)
		printf(" %d", heaparray[i]);
	printf("\n");

	free(heaparray);
}

int main()
{
	test(quicksort_c, "c");
	test(quicksort_asm, "asm");
	return 0;
}
