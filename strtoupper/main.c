#define _POSIX_C_SOURCE 200809L

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "curtime.h"

extern void toupper_asm(char* str);
extern void toupper_simd(char* str);

void toupper_c(char* text) {
    for (; *text != '\0'; text++)
        if (*text >= 'a' && *text <= 'z')
            *text -= 0x20;
}

static inline void run(const char* name, void (*func)(char*), char* arg) {
    double start = curtime();
    func(arg);
    double end = curtime();
    printf("%-4s took %f seconds\n", name, end - start);
}

struct Variant {
    const char* name;
    void (*func)(char*);
};

typedef struct Variant Variant;

Variant variants[] = {
    { "c", toupper_c },
    { "asm", toupper_asm },
    { "simd", toupper_simd },
};

const char prefix[] = "abcxyzABCXYZ01`{";

int main(int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: %s <length> <variant>\n", argv[0]);
        for (size_t i = 0; i < sizeof(variants) / sizeof(Variant); i++)
            printf(" Variant %d: %s\n", i, variants[i].name);
        return 1;
    }

    size_t length = atoi(argv[1]);
    unsigned variant = atoi(argv[2]);

    if (variant >= sizeof(variants) / sizeof(Variant)) {
        printf("Invalid variant: %u\n", variant);
        return 1;
    }

    char* string = malloc(length + 4);

    strncpy(string, prefix, length);

    size_t prefix_len = strlen(prefix);
    if (length > prefix_len) {
        // Generate a random string
        int fd = open("/dev/urandom", O_RDONLY);
        read(fd, string + prefix_len, length - prefix_len);
        close(fd);

        unsigned* words = (unsigned*) (string + prefix_len);
        for (size_t i = 0; i < length / 4 + 1; i++)
            words[i] = (words[i] | 0x40404040) & ~0x90909090; // valid ASCII
    }

    string[length] = 0; // Terminate

    Variant* variantStruct = &variants[variant];

    printf("Before: %.50s\n", string);
    run(variantStruct->name, variantStruct->func, string);
    printf("After:  %.50s\n", string);

    return 0;
}

